import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:neekonazaninplayer/src/sona_player_options.dart';

import 'package:neekonazaninplayer/src/sona_player_widget.dart';
import 'package:neekonazaninplayer/src/video-state.dart';
import 'package:neekonazaninplayer/src/video_controller_wrapper.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
          fontFamily: "iransans",
          primarySwatch: Colors.grey,
          primaryColor:Color.fromRGBO(224, 224, 224, 1) ,
          accentColor: Color.fromRGBO(200, 50, 51, 1),
          backgroundColor: Color.fromRGBO(18 , 18, 18, 1),
          textSelectionColor: Color.fromRGBO(224, 224, 224, 1).withOpacity(0.5),
          textSelectionHandleColor: Color.fromRGBO(160, 160, 160, 1),
          cardColor: Color.fromRGBO(54, 54, 54, 1),//363636
          disabledColor:  Color.fromRGBO(108, 108, 108, 1),
          unselectedWidgetColor: Colors.grey,

      ),
      home: MyHomePage(title: 'Neeko Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  void initState() {
    VideoState.init();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: RaisedButton(
          onPressed: (){

              Navigator.of(context).push(MaterialPageRoute(builder: (context) => VideoPlayer(),));

          },
        ),
      )
    );
  }
}

class VideoPlayer extends StatefulWidget {
  @override
  _VideoPlayerState createState() => _VideoPlayerState('https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4');
}

class _VideoPlayerState extends State<VideoPlayer> {
  bool loading = false;
  VideoControllerWrapper videoControllerWrapper;
  String video;

  _VideoPlayerState(this.video) {
    videoControllerWrapper = VideoControllerWrapper(
        DataSource.network(video, displayName: "IBA video"), setSTatecallback);

  }
  void setSTatecallback() {

    setState(() {

    });
  }
  void onrefresh(){
    videoControllerWrapper = VideoControllerWrapper(
        DataSource.network(video, displayName: "IBA video"), setSTatecallback);

  }

//  final VideoControllerWrapper videoControllerWrapper = VideoControllerWrapper(
//      DataSource.network(
////          'https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4',
//          'https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_5MG.mp3',
//          displayName: "displayName"));


  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
  }

  @override
  void dispose() {
    print("video player main called dispose!");
    VideoState.instance.resetVideoTimer();
    SystemChrome.restoreSystemUIOverlays();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {

    });
    return Scaffold(
      body: Stack(
        children: [
           SoNaPlayerWidget(
             videoControllerWrapper: videoControllerWrapper,
             progressBarBufferedColor:
             Theme.of(context).textSelectionHandleColor,
             progressBarBackgroundColor:
             Theme.of(context).unselectedWidgetColor.withOpacity(0.5),
             progressBarPlayedColor: Theme.of(context).accentColor,
             progressBarHandleColor: Theme.of(context).primaryColor,
             controllerTimeout: Duration(milliseconds: 3000),
             playerOptions: SoNaPlayerOptions(
               loop: true,
               useController: true,
               enabledSystemUIOverlaysWhenEnterLandscape: [
                 SystemUiOverlay.bottom
               ],
             ),
             actions: [
               GestureDetector(onTap:(){
                 Navigator.of(context).pop();
               },child: Container(width:50,height: 50 ,color:Colors.black54,child: Icon(Icons.close))),
             ],
           ),
            videoControllerWrapper.error != ""
              ?AspectRatio(aspectRatio: 16/9,
                child: Center(
                  child: IconButton(
                    icon: Icon(
                      Icons.refresh,
                      color: Theme.of(context).accentColor,
                      size: 40,
                    ),
                    onPressed: () {
                      setState(() {});
                      videoControllerWrapper = VideoControllerWrapper(
                          DataSource.network(video,
                              displayName: "IBA video"),setSTatecallback);
                      setState(() {});

                    },
                  ),
                ),
              ):Container(),
            videoControllerWrapper.loading
                ? AspectRatio(aspectRatio: 16/9,child: Center(child: CircularProgressIndicator()),
              )
              : Container(),
            ],

      )
    );
  }
}

