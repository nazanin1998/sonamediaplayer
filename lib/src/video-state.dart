import 'package:neekonazaninplayer/src/video_controller_wrapper.dart';
import 'package:video_player/video_player.dart';

class VideoState {
  static VideoState instance;
  Duration _videoDuration=Duration(seconds: 0);
  VideoPlayerController controller;

  static  init() async {
    instance = new VideoState();
  }
  void setVideoTimer(Duration videoDuration)
  {
    _videoDuration = videoDuration;
  }
  Duration getVideoTimer()
  {
    return _videoDuration ;

  }

  void resetVideoTimer()
  {
    _videoDuration=Duration(seconds: 0);
    controller=null;
  }

  VideoPlayerController  getVideoController()
  {
    return controller ;
  }

  void setVideoController(VideoPlayerController controller)
  {
    print(controller);
    this.controller = controller;

  }
}