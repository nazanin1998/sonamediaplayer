//Copyright (c) 2019 Neeko Contributors
//
//Neeko is licensed under the Mulan PSL v1.
//
//You can use this software according to the terms and conditions of the Mulan PSL v1.
//You may obtain a copy of Mulan PSL v1 at:
//
//http://license.coscl.org.cn/MulanPSL
//
//THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
//PURPOSE.
//
//See the Mulan PSL v1 for more details.

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:neekonazaninplayer/src/video-state.dart';
import 'package:video_player/video_player.dart';

import 'duration_formatter.dart';
import 'sona_player_options.dart';
import 'progress_bar.dart';
import 'video_controller_wrapper.dart';

class TouchShutter extends StatefulWidget {
  final VideoControllerWrapper controllerWrapper;
  final ValueNotifier<bool> showControllers;
  final bool enableDragSeek;

  const TouchShutter(this.controllerWrapper,
      {Key key, this.showControllers, this.enableDragSeek})
      : super(key: key);

  @override
  _TouchShutterState createState() => _TouchShutterState();
}

class _TouchShutterState extends State<TouchShutter> {
  double dragStartPos = 0.0;
  double delta = 0.0;
  int seekToPosition = 0;
  String seekDuration = "";
  String seekPosition = "";

  bool _dragging = false;

  VideoPlayerController get controller => widget.controllerWrapper.controller;

  @override
  void initState() {
    super.initState();
    widget.showControllers.addListener(() {
      if (mounted) setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    if (controller == null) {
      return Container();
    }

    return widget.enableDragSeek
        ? GestureDetector(
            onTap: () =>
                widget.showControllers.value = !widget.showControllers.value,
            onHorizontalDragStart: (details) {
              setState(() {
                _dragging = true;
              });
              dragStartPos = details.globalPosition.dx;
            },
            onHorizontalDragUpdate: (details) {
              delta = details.globalPosition.dx - dragStartPos;
              seekToPosition =
                  (controller.value.position.inMilliseconds + delta * 1000)
                      .round();
              setState(() {
                seekDuration = (delta < 0 ? "- " : "+ ") +
                    durationFormatter(
                        (delta < 0 ? -1 : 1) * (delta * 1000).round());
                if (seekToPosition < 0) seekToPosition = 0;
                seekPosition = durationFormatter(seekToPosition);
              });
            },
            onHorizontalDragEnd: (_) {
              controller.seekTo(Duration(milliseconds: seekToPosition));
              setState(() {
                _dragging = false;
              });
            },
            child: AnimatedContainer(
              duration: Duration(milliseconds: 300),
              decoration: BoxDecoration(
                color: Colors.transparent,
              ),
              child: _dragging
                  ? Center(
                      child: Container(
                        padding: EdgeInsets.all(4.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          color: Colors.black.withAlpha(150),
                        ),
                        child: Text(
                          "$seekDuration ($seekPosition)",
                          style: TextStyle(
                            fontSize: 26.0,
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )
                  : Container(),
            ),
          )
        : GestureDetector(
            onTap: () =>
                widget.showControllers.value = !widget.showControllers.value,
          );
  }
}

class TopBar extends StatefulWidget {
  final VideoControllerWrapper controllerWrapper;
  final List<Widget> actions;
  final ValueNotifier<bool> showControllers;
  final Widget leading;
  final SoNaPlayerOptions options;
  final Function onPortraitBackTap;
  final Function onLandscapeBackTap;

  final bool isFullscreen;

  const TopBar(this.controllerWrapper,
      {Key key,
      this.showControllers,
      this.actions,
      this.leading,
      this.options,
      this.onPortraitBackTap,
      this.onLandscapeBackTap,
      this.isFullscreen = false})
      : super(key: key);

  @override
  _TopBarState createState() => _TopBarState();
}

class _TopBarState extends State<TopBar> {
  VideoControllerWrapper _controllerWrapper;

  VideoPlayerController get controller => _controllerWrapper.controller;

  set controllerWrapper(VideoControllerWrapper controllerWrapper) =>
      _controllerWrapper = controllerWrapper;

  @override
  void initState() {
    super.initState();
    widget.showControllers.addListener(() {
      if (mounted) setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: true,
      child: Padding(
        padding: EdgeInsets.only(
            left: 2.0, right: 2.0, top: MediaQuery.of(context).padding.top),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[

            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: widget.actions ?? [Container()],
              ),
              flex: 3,
            )
          ],
        ),
      ),
    );
  }


}

class BottomBar extends StatefulWidget {
  final VideoControllerWrapper controllerWrapper;
  final Color playedColor;
  final Color bufferedColor;
  final Color handleColor;
  final Color backgroundColor;
  final double aspectRatio;
  final ValueNotifier<bool> showControllers;

  final bool isFullscreen;

  final Function onEnterFullscreen;
  final Function onExitFullscreen;

  const BottomBar(this.controllerWrapper,
      {Key key,
      this.playedColor,
      this.bufferedColor,
      this.handleColor,
      this.backgroundColor,
      this.aspectRatio,
      this.showControllers,
      this.isFullscreen = false,
      this.onEnterFullscreen,
      this.onExitFullscreen})
      : super(key: key);

  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar>  with SingleTickerProviderStateMixin {

  int _currentPosition = 0;
  int _duration = 0;

  bool _isPlaying = false;

  VideoControllerWrapper _controllerWrapper;

  VideoPlayerController get controller => widget.controllerWrapper.controller;

  set controllerWrapper(VideoControllerWrapper controllerWrapper) =>
      _controllerWrapper = controllerWrapper;

  AnimationController _animController;

  @override
  void initState() {
    super.initState();
    _controllerWrapper = widget.controllerWrapper;
    _controllerWrapper.addListener(() {
      _attachListenerToController();
    });
    widget.showControllers.addListener(
          () {
        if (mounted) setState(() {});
      },
    );

    _animController = AnimationController(
      vsync: this,
      value: 0,
      duration: Duration(milliseconds: 300),
    );

  }

  @override
  dispose() {
    _animController.dispose();
    super.dispose();
  }

  _attachListenerToController() {
    controller?.addListener(_videoControllerListener);
  }

  _videoControllerListener() {
    if (!mounted) {
      return;
    }
    if (controller.value.duration == null ||
        controller.value.position == null) {
      return;
    }

    setState(() {
      _isPlaying = controller.value.isPlaying;
    });
    if (mounted) {
      setState(() {
        _currentPosition = controller.value.duration.inMilliseconds == 0
            ? 0
            : controller.value.position.inMilliseconds;
        _duration = controller.value.duration.inMilliseconds;
      });
    }
    if (controller.value.isPlaying) {
      _animController.forward();
    } else {
      _animController.reverse();
    }
  }

  _removeVideoControllerListener() {
    controller?.removeListener(_videoControllerListener);
  }

  @override
  Widget build(BuildContext context) {
    _removeVideoControllerListener();
    _attachListenerToController();

    return Material(

      color: Colors.transparent,
      child: Column(

        children: [
          Row(

              children: <Widget>[
                  SizedBox(
                    height: 30,
                    width: 20,
                  ),
                Text(
                  durationFormatter(_currentPosition),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14.0,
                  ),
                ),


                if(!_controllerWrapper.dataSource.dataSource.contains(".mp3"))
                Expanded(child: Row(),),
                InkWell(
                  borderRadius: BorderRadius.circular(50.0),
                  onTap: _play,
                  child: AnimatedIcon(
                    icon: AnimatedIcons.play_pause,
                    progress: _animController.view,
                    color: Colors.white,
                    size: 35,
                  ),
                ),

                Expanded(child: Row(),),

                Text(
                  "${durationFormatter(_duration)}",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14.0,
                  ),
                ),

                if(!_controllerWrapper.dataSource.dataSource.contains(".mp3"))

                IconButton(
                  icon: Icon(
                    widget.isFullscreen ? Icons.fullscreen_exit : Icons.fullscreen,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    if (controller == null || !controller.value.initialized) {
                      return;
                    }
                    if (widget.isFullscreen && widget.onExitFullscreen != null) {
                      widget.onExitFullscreen();
                    } else if (!widget.isFullscreen &&
                        widget.onEnterFullscreen != null) {
                      widget.onEnterFullscreen();
                    }
                  },
                ),

              ],
          ),

          ProgressBar(
            _controllerWrapper,
            showControllers: widget.showControllers,
            playedColor: widget.playedColor,
            handleColor: widget.handleColor,
            backgroundColor: widget.backgroundColor,
            bufferedColor: widget.bufferedColor,
          ),
        ],
      ),
    );
  }

  _play() async {
    if(controller==null){
      return;
    }
    if (!controller.value.initialized) {
      if (controller.value.position != null) {
        await controller.initialize();
        _attachListenerToController();
        await controller.seekTo(
            VideoState.instance.getVideoTimer()
        );
        await controller.play();
      }
      return;
    }

    if (_isPlaying) {
      controller.pause();
    } else {

      if (controller.value.position == null) {
        controller.play();
      } else if (controller.value.position.inMilliseconds >=
          controller.value.duration.inMilliseconds) {
        await controller.seekTo(Duration(seconds: 0));
        await controller.play();
      } else {
        controller.play();
      }
    }

    _animate();
  }
  _animate() {}

}
