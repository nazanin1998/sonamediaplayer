//Copyright (c) 2019 Neeko Contributors
//
//Neeko is licensed under the Mulan PSL v1.
//
//You can use this software according to the terms and conditions of the Mulan PSL v1.
//You may obtain a copy of Mulan PSL v1 at:
//
//http://license.coscl.org.cn/MulanPSL
//
//THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
//PURPOSE.
//
//See the Mulan PSL v1 for more details.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:neekonazaninplayer/src/progress_bar.dart';
import 'package:video_player/video_player.dart';

import 'sona_fullscreen_player.dart';
import 'sona_player.dart';
import 'sona_player_options.dart';
import 'video_controller_widgets.dart';
import 'video_controller_wrapper.dart';

///core video player
class SoNaPlayerWidget extends StatefulWidget {
  final VideoControllerWrapper videoControllerWrapper;

  final SoNaPlayerOptions playerOptions;
  final double width;
  final Duration controllerTimeout;
  final Widget bufferIndicator;
  final double aspectRatio;
  final List<Widget> actions;
  final Duration startAt;
  final bool inFullScreen;
  final Function onPortraitBackTap;


  final Color progressBarPlayedColor;
  final Color progressBarBufferedColor;
  final Color progressBarHandleColor;
  final Color progressBarBackgroundColor;

  SoNaPlayerWidget(
      {Key key,
      @required this.videoControllerWrapper,
      this.playerOptions = const SoNaPlayerOptions(),
      this.controllerTimeout = const Duration(seconds: 3),
      this.bufferIndicator,
      this.aspectRatio = 16 / 9,
      this.width,
      this.actions,
      this.startAt = const Duration(seconds: 0),
      this.inFullScreen = false,
      this.onPortraitBackTap,

      this.progressBarPlayedColor,
      this.progressBarBufferedColor: const Color(0xFF757575),
      this.progressBarHandleColor,
      this.progressBarBackgroundColor: const Color(0xFFF5F5F5)})
      : assert(videoControllerWrapper != null),
        assert(playerOptions != null),
        super(key: key);

  @override
  _SoNaPlayerWidgetState createState() => _SoNaPlayerWidgetState();
}

class _SoNaPlayerWidgetState extends State<SoNaPlayerWidget> {
  final _showControllers = ValueNotifier<bool>(false);

  Timer _timer;

  VideoPlayerController get controller =>
      widget.videoControllerWrapper.controller;

  VideoControllerWrapper get videoControllerWrapper =>
      widget.videoControllerWrapper;

  @override
  void initState() {
    super.initState();
    _addShowControllerListener();
    _listenVideoControllerWrapper();
  }

  void _listenVideoControllerWrapper() {
    videoControllerWrapper.addListener(() {
      print("video wrapper");
      if (mounted)
        setState(() {
        });
    });
  }

  void _addShowControllerListener() {
    _showControllers.addListener(() {
      print("video _addShowControllerListener");

      _timer?.cancel();
      if (_showControllers.value) {
        _timer = Timer(
          widget.controllerTimeout,
          () => _showControllers.value = false,
        );
      }
    });
  }

  @override
  void dispose() {

//    _showControllers.dispose();
    controller?.dispose();
    videoControllerWrapper?.dispose();
    _timer?.cancel();
    super.dispose();
  }

  void pushFullScreenWidget() {
    final TransitionRoute<void> route = PageRouteBuilder<void>(
      settings: RouteSettings(name: "neeko_full"),
      pageBuilder: (BuildContext context, Animation<double> animation,
              Animation<double> secondaryAnimation) =>
          fullScreenRoutePageBuilder(
        context: context,
        animation: animation,
        secondaryAnimation: secondaryAnimation,
        videoControllerWrapper: widget.videoControllerWrapper,
        width: widget.width,
        actions: widget.actions,
        aspectRatio: widget.aspectRatio,
        bufferIndicator: widget.bufferIndicator,
        controllerTimeout: widget.controllerTimeout,
        playerOptions: SoNaPlayerOptions(
            enableDragSeek: widget.playerOptions.enableDragSeek,
            showFullScreenButton: widget.playerOptions.showFullScreenButton,
            autoPlay: true,
            useController: widget.playerOptions.useController,
            preferredOrientationsWhenEnterLandscape:
                widget.playerOptions.preferredOrientationsWhenEnterLandscape,
            preferredOrientationsWhenExitLandscape:
                widget.playerOptions.preferredOrientationsWhenExitLandscape,
            enabledSystemUIOverlaysWhenEnterLandscape:
                widget.playerOptions.enabledSystemUIOverlaysWhenEnterLandscape,
            enabledSystemUIOverlaysWhenExitLandscape:
                widget.playerOptions.enabledSystemUIOverlaysWhenExitLandscape),
      ),
    );

    route.completed.then((void value) {
//      controller.setVolume(0.0);
    });

//    controller.setVolume(1.0);
    Navigator.of(context).push(route).then((_) {
      if (mounted)
        setState(() {
          _listenVideoControllerWrapper();
        });
    });
  }

  @override
  Widget build(BuildContext context) {

    return Hero(
      tag: "com.jarvanmo.neekoPlayerHeroTag",
      child: Container(
        width: widget.width ?? MediaQuery.of(context).size.width,
        child: AspectRatio(
          aspectRatio: widget.aspectRatio,
          child: Stack(
            fit: StackFit.expand,
            overflow: Overflow.visible,
            children: <Widget>[

              SoNaPlayer(controllerWrapper: videoControllerWrapper),
              if(videoControllerWrapper.dataSource.dataSource.contains(".mp3"))
                Container( color:Colors.green,  width: MediaQuery.of(context).size.width,),

              if (widget.playerOptions.useController)
                TouchShutter(
                  videoControllerWrapper,
                  showControllers: _showControllers,
                  enableDragSeek: widget.playerOptions.enableDragSeek,
                ),

              if (widget.playerOptions.useController)
                Positioned(
                    left: 0,
                    right: 0,
                    top: 0,
                    child: TopBar(
                      videoControllerWrapper,
                      showControllers: _showControllers,
                      options: widget.playerOptions,
                      actions: widget.actions,
                      isFullscreen: false,
                      onPortraitBackTap: widget.onPortraitBackTap,
                    )),

                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child:BottomBar(
                          videoControllerWrapper,
                          aspectRatio: widget.aspectRatio,
                          showControllers: _showControllers,
                          playedColor: widget.progressBarPlayedColor,
                          handleColor: widget.progressBarHandleColor,
                          backgroundColor: widget.progressBarBackgroundColor,
                          bufferedColor: widget.progressBarBufferedColor,
                          isFullscreen: false,
                          onEnterFullscreen: pushFullScreenWidget,
                        ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
