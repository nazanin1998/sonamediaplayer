//Copyright (c) 2019 Neeko Contributors
//
//Neeko is licensed under the Mulan PSL v1.
//
//You can use this software according to the terms and conditions of the Mulan PSL v1.
//You may obtain a copy of Mulan PSL v1 at:
//
//http://license.coscl.org.cn/MulanPSL
//
//THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
//PURPOSE.
//
//See the Mulan PSL v1 for more details.

import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:neekonazaninplayer/src/video-state.dart';
import 'package:neekonazaninplayer/src/video-state.dart';
import 'package:video_player/video_player.dart';

class VideoControllerWrapper extends ValueNotifier<DataSource> {
  VideoPlayerController get controller => _videoPlayerController;

  List<VideoPlayerController> _controllerPool = [];

  VideoPlayerController _videoPlayerController;

  DataSource _dataSource;
  bool loading = true;
  String error = "";
  final VoidCallback refreshParentState;

  VideoControllerWrapper(DataSource value,this.refreshParentState) : super(value) {
    prepareDataSource(value);
  }
  ///Get current [dataSource]
  DataSource get dataSource => _dataSource;

  ///Prepare your [dataSource] and initialize [_videoPlayerController].
  ///Old controllers will be disposed once the one is under buffering or playing.
  Future prepareDataSource(DataSource dataSource) async {
    _dataSource = dataSource;

    await _videoPlayerController?.pause();

    VideoPlayerController newController;
    switch (dataSource.dataSourceType) {
      case DataSourceType.asset:
        newController = VideoPlayerController.asset(dataSource.dataSource,
            package: dataSource.package);
        break;
      case DataSourceType.network:
        newController = VideoPlayerController.network(dataSource.dataSource);
        break;
      case DataSourceType.file:
        newController = VideoPlayerController.file(File(dataSource.dataSource));
        break;
    }

    newController.addListener(_videoControllerListener);
    newController.initialize().then((value){
      loading = false;
      Duration _dur = VideoState.instance.getVideoTimer();
      if(_dur.inSeconds!=0)
        newController.seekTo(_dur);

      _controllerPool.add(
          _videoPlayerController); // add the old one into pool then dispose it.
      _videoPlayerController = newController;
      VideoState.instance.setVideoController(_videoPlayerController);
      notifyListeners();
      refreshParentState();
    }).catchError((err){
      loading = false;
      refreshParentState();
      error = err.toString();
    });

  }

  _videoControllerListener() {
    if (_videoPlayerController == null ||
        !_videoPlayerController.value.initialized) {
      return;
    }else{
      print("set video controller");
      VideoState.instance.setVideoController(_videoPlayerController);
    }
    if (_videoPlayerController.value.isPlaying) {
      VideoState.instance.setVideoTimer(_videoPlayerController.value.position);
      print("======================= Timer ====================");
      print(VideoState.instance.getVideoTimer());
    }

  }
}

class DataSource {
  final String dataSource;
  final DataSourceType dataSourceType;
  final String package;
  final String displayName;
  final String subtitle;
  final dynamic id;
  final Map extras;

  DataSource.network(this.dataSource,
      {this.displayName, this.id, this.extras, this.subtitle})
      : package = null,
        dataSourceType = DataSourceType.network;

  DataSource.file(File file,
      {this.displayName, this.id, this.extras, this.subtitle})
      : dataSource = '${file.path}',
        package = null,
        dataSourceType = DataSourceType.file;

  DataSource.asset(this.dataSource,
      {this.package, this.displayName, this.id, this.extras, this.subtitle})
      : dataSourceType = DataSourceType.asset;
}
